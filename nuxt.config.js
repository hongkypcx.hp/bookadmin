
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel:"stylesheet", href:"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", integrity:"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh", crossorigin:"anonymous"}
    ],
    script:[
      {src:"https://code.jquery.com/jquery-3.4.1.slim.min.js"},
      {src:"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"},
      {src:"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"},
      // {src:"https://www.gstatic.com/firebasejs/7.14.1/firebase-app.js"},
      // {src:"https://www.gstatic.com/firebasejs/7.14.1/firebase-analytics.js"},
      // {src:"https://www.gstatic.com/firebasejs/7.14.1/firebase-database.js"},
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/tailwind.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~/plugins/vue2-editor", ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  modulesDir:[

  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  /**
   * Gitlab
   */
  router: {
    base: '/bookadmin/',
  },
  generate: {
    dir: 'public',
  },
  // router:{
  //   extendRoutes (routes, resolve) {
  //     // routes.push({
  //     //
  //     // })
  //   }
  // }
}
