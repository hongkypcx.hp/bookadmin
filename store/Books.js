import * as firebase from "firebase"
import Vue from 'vue'
import Vuex from 'vuex'
var firebaseConfig = {
  apiKey: "AIzaSyBPogJdQIHhfX3VV-U8vXmin2XowMp_cQ4",
  authDomain: "xreader-d91b9.firebaseapp.com",
  databaseURL: "https://xreader-d91b9.firebaseio.com",
  projectId: "xreader-d91b9",
  storageBucket: "xreader-d91b9.appspot.com",
  messagingSenderId: "886091773507",
  appId: "1:886091773507:web:5b211ce15817e76bde86f6",
  measurementId: "G-GTYRBFM8EV"
};
// // Initialize Firebase
export const firebaseApp = !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();



export const state = () => ({
  books:[]
})

export const mutations = {
  addBooks(state,books){
    state.books = books

  },
  ADD_CHAPTER(state,payload){
    var index = state.books.findIndex(function (e) {
      return e.id == payload.book.id
    })
    state.books[index].chapters.push(payload.chapter)
  },
  ADD_BOOK(state,payload){
    console.log('payload',payload,'state',state.books)
    state.books[payload.id]=payload
  }

}
export const actions = {
   fetchBooks(context){
    var database = firebaseApp.database();
    return database.ref('/books').once('value').then(function(snapshot) {
      let books = snapshot.exportVal()
        console.log('books',books)
      context.commit('addBooks',books)
    })
  },
  getBookById(context,bookId){
    const path = '/books/'+bookId
    var database = firebaseApp.database();
    return database.ref(path).once('value')
  },addBook(context,payload){
    var database = firebaseApp.database();
    const path = '/books/'
    var bookId = database.ref(path).push().key
    var updates = {}
    payload["id"] = bookId
    updates[bookId]=payload
    database.ref(path).update(updates)
    return bookId

  },
  deleteBook(context,payload){
    const path = '/books/'+payload.bookId
    console.log('book id',path)
    var database = firebaseApp.database();
    database.ref(path).remove().then(function(snapshot) {
      context.dispatch('fetchBooks')
    })
  },
  updateBook(context,payload){
    const path = '/books/'+payload.bookId
    console.log('book id',path)
    var database = firebaseApp.database();
    var updates = {
      title:payload.book.title,
      description:payload.book.description,
      image:payload.book.image
    }
    database.ref(path).update(updates).then(function(snapshot) {
      context.dispatch('fetchBooks')
    })
  },
  updateChapter(context,payload){
     const path = '/books/'+payload.bookId+'/chapters/'+payload.chapter.id
    console.log('book id',path)
    var database = firebaseApp.database();
    database.ref(path).set(payload.chapter).then(function(snapshot) {
      context.dispatch('fetchBooks')
    })
  },
  addChapter(context,payload){
    var database = firebaseApp.database();
    var bookId = payload.bookId
     if (typeof bookId === "undefined"){
       bookId=database.ref('/books').push().key
     }
    const path = '/books/'+bookId+'/chapters/'
    console.log('book id',path)

    var id = database.ref(path).push().key
    payload.chapter['id'] = id
    database.ref(path+id).set(payload.chapter).then(function () {
      context.dispatch('fetchBooks')
    })
    var book = database.ref('/books/'+bookId)
  },
  deleteChapter(context,payload){
    const path = '/books/'+payload.bookId+'/chapters/'+payload.chapter.id
    console.log('book id',path)
    var database = firebaseApp.database();
    database.ref(path).remove().then(function(snapshot) {
      context.dispatch('fetchBooks')
    })
  }
}
export const getters = {
  getBooks:state => {
    return state.books
  },getBookRF:state=>bookId=>{
    const path = '/books/'+bookId
    var database = firebaseApp.database();
    return database.ref(path)
  }


}



